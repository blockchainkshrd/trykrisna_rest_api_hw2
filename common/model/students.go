package model

type Response struct {
	Record []interface{} `json:"record"`
}

type Students struct {
	Id             string      `json:"_id,omitempty"`
	Rev            string      `json:"_rev,omitempty"`
	Studentid      int         `json:"studentid"`
	Studentname    string      `json:"studentname"`
	Gender         string      `json:"gender"`
	Info           interface{} `json:"info"`
	IsClassMonitor bool        `json:"is_class_monitor"`
	Classname      string      `json:"classname"`
	Teamname       string      `json:"teamname"`
}
