package response

import (
	"github.com/gin-gonic/gin"
	"time"
)

var currentTime = time.Now()

func NotFoundResponse(msg string, code int) gin.H {
	return gin.H{
		"Message":      msg,
		"code":         code,
		"Request Time": currentTime.Format("01-02-2006"),
	}
}

func ScuccessFoundResponse(msg string, code int, T any) gin.H {
	return gin.H{
		"Message":      msg,
		"code":         code,
		"Request Time": currentTime.Format("01-02-2006"),
		"Data":         T,
	}
}

func ErrorResponse(msg string, code int, err error) gin.H {
	return gin.H{
		"Message":      msg,
		"code":         code,
		"Request Time": currentTime.Format("01-02-2006"),
		"Errors":       err,
	}
}
