package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-kivik/kivik/v3"
	"go/common/config"
	"go/common/model"
	"go/common/response"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var currentTime = time.Now()

func FindById(c *gin.Context) {
	var responses model.Students
	type Vehicle struct {
	}
	err := config.DB.Get(context.TODO(), c.Param("docId")).ScanDoc(&responses)
	if err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorResponse("There are problem while trying to get data from database", 400, err))
	} else {
		if (model.Students{} == responses) {
			c.JSON(http.StatusNotFound, response.NotFoundResponse("Cannot find any student with this id", 404))
		} else {
			c.JSON(http.StatusBadRequest, response.ScuccessFoundResponse("Successfully fetch data", 200, responses))
		}
	}
}

func FindByRevId(c *gin.Context) {
	revId := c.Param("rev")
	var stu model.Students
	rows, err := config.DB.Query(context.TODO(), "_design/getdoc", "_view/get-by-rev", kivik.Options{"key": revId, "include_docs": true})
	if err != nil {
		//panic(err)
	}
	for rows.Next() {
		if err := rows.ScanDoc(&stu); err != nil {
			panic(err)
		}
	}
	if (model.Students{} == stu) {
		c.JSON(http.StatusNotFound, response.NotFoundResponse("Cannot find any student with this revision id", 404))
	} else {
		c.JSON(http.StatusOK, response.ScuccessFoundResponse("Successfully fetch data", 200, stu))
		//c.JSON(http.StatusOK, gin.H{"Message": "Successfully fetch data", "code": "200", "Request Time": currentTime.Format("01-02-2006"), "data": stu})
	}

}

func DeleteDocById(c *gin.Context) {
	newRev, err := config.DB.Delete(context.TODO(), c.Param("docId"), c.Param("revId"))
	fmt.Printf("", newRev)
	if err != nil {
		panic(err)

	} else {
		c.JSON(http.StatusOK, gin.H{"Message": "successfully delete student", "code": "200", "Request Time": currentTime.Format("01-02-2006"), "deleted student": newRev})
	}
}

func FilterByClass(c *gin.Context) {
	classname := c.Param("classname")
	var stu model.Students
	var newarr []interface{}
	rows, err := config.DB.Query(context.TODO(), "_design/getdoc", "_view/filter-by-class", map[string]interface{}{"key": classname, "include_docs": true, "reduce": false})
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		if err := rows.ScanDoc(&stu); err != nil {
			panic(err)
			c.JSON(http.StatusBadRequest, response.ErrorResponse("There are problem while trying to get data from database", 300, err))
		}
		newarr = append(newarr, stu)
	}
	if len(newarr) == 0 {
		c.JSON(http.StatusNotFound, response.NotFoundResponse("Cannot find any student with this class name !!!", 404))
	} else {
		c.JSON(http.StatusOK, response.ScuccessFoundResponse("Successfully fetch data", 200, newarr))
	}

}

func AddStudent(c *gin.Context) {
	res, err := http.Get("https://api.jsonbin.io/v3/b/63155201a1610e63861e772b")
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		c.JSON(http.StatusOK, response.ErrorResponse("Error while trying to fetch data", 400, err))
		log.Fatal(err)
	}
	var responses model.Response
	json.Unmarshal(body, &responses)
	type Animal interface {
		Speak() string
	}
	config.DB.BulkDocs(context.TODO(), responses.Record)
	c.JSON(http.StatusOK, response.ScuccessFoundResponse("", 200, "Insert Successfully ..."))
}

func UpdateDocById(c *gin.Context) {
	var newStu model.Students
	c.BindJSON(&newStu)
	fmt.Printf("new stu", newStu.Classname)
	var responses model.Students
	err := config.DB.Get(context.TODO(), c.Param("docId")).ScanDoc(&responses)
	if err != nil {

		c.JSON(http.StatusBadRequest, response.ErrorResponse("Error found", 400, err))
	} else {

		newStu.Rev = responses.Rev
		if newStu.Studentid == 0 {
			newStu.Studentid = responses.Studentid
		} else if newStu.Studentname == "" {
			newStu.Studentname = responses.Studentname
		} else if newStu.Gender == "" {
			newStu.Gender = responses.Gender
		} else if newStu.Info == nil {
			newStu.Info = responses.Info
		} else if newStu.Classname == "" {
			newStu.Classname = responses.Classname
		} else if newStu.Teamname == "" {
			newStu.Teamname = responses.Teamname
		}
		newRev, err := config.DB.Put(context.TODO(), c.Param("docId"), newStu)
		if err != nil {
			c.JSON(http.StatusOK, response.ErrorResponse("Error while trying to fetch data", 400, err))
		}
		c.JSON(http.StatusOK, response.ScuccessFoundResponse("successfully Update student info", 200, newRev))
	}
}
